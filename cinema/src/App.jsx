import { useState } from 'react'
import { Footer } from './components/footer.jsx'
import { Header } from './components/Header.jsx'
import { Ticket } from './components/Ticket.jsx'
import { Filtro } from './components/Filtro.jsx'
import { CardFilme } from './components/CardFilme.jsx'
import { Horarios } from './components/Horarios.jsx'
import './App.css'
import './global.css'
import { FaleConosco } from './pages/faleconosco.jsx'
import { Registro } from './pages/registro.jsx'
import { Home } from './pages/Home.jsx'
import { Filmes } from './pages/Filmes.jsx'
import { Login } from './pages/Login.jsx'
import { Sessoes } from './pages/Sessoes.jsx'

//Rotas
import {Router} from './Router.jsx'

import { BrowserRouter, Route, Routes } from 'react-router-dom'


function App() {

  return (
    <BrowserRouter>
      <Router/>
    </BrowserRouter>

  )
}

export default App
