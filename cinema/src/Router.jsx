import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { FaleConosco } from './pages/faleconosco.jsx'
import { Home } from './pages/Home.jsx'
import { Filmes } from './pages/Filmes.jsx'
import { Sessoes } from './pages/Sessoes.jsx'

import { Checkout } from './pages/Checkout.jsx'
import { Registro } from './pages/registro.jsx'
import { Login} from './pages/Login.jsx'

export function Router() {
    return (
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/Filmes" element={<Filmes />} />
            <Route path="/Sessoes" element={<Sessoes />} />
            <Route path="/FaleConosco" element={<FaleConosco />} />
            <Route path='/Checkout' element={<Checkout />} />
            <Route path='/Registro' element={<Registro />} />
            <Route path='/Login' element={<Login />} />
        </Routes>
    )
}