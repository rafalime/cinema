import styles from './cardfilme.module.css'
import { NavLink } from 'react-router-dom'

export function CardFilme(props) {

    

    return (
        <div className={styles.CardFilme}>
            <img src={props.url} alt="Cartaz do filme" />
            <div className={styles.NameAge}>
                <h2>{props.nome}</h2>
                <div><p>{props.classe}</p></div>
            </div>
            <div className={styles.InfoFilme}>
                <p>{props.genero}</p>
                <p>Direção: {props.diretor}</p>
                <p>{props.sinopse}</p>
            </div>
            <NavLink to='/Sessoes'><button className={styles.SeeBtn}>VER SESSÕES</button></NavLink>
        </div>
    )
}