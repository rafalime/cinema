import styles from './filtro.module.css'
import Lupa from '../_assets/icons/Lupa.png'


export function Filtro() {
    return (
        <div className={styles.filter}>
            <div className={styles.SearchFilter}>
                <input type="text" placeholder='Pesquisar filmes' />
                <button><img src={Lupa} alt="" /></button>
            </div>
            <div className={styles.SelectionFilter}>
                <select name="" id="">
                    <option value="">Animação</option>
                    <option value="">Ação</option>
                    <option value="">Aventura</option>
                    <option value="">Comédia</option>
                    <option value="">Drama</option>
                    <option value="">Fantasia</option>

                </select>
                <select name="" id="">
                    <option value="">Livre</option>
                    <option value="">10</option>
                    <option value="">12</option>
                    <option value="">14</option>
                    <option value="">16</option>
                    <option value="">18</option>
                </select>
            </div>


        </div>
    )
}