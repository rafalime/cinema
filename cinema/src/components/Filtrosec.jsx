import styles from './filtrosec.module.css'

export function Filtrosec() {
    return (
        <div className={styles.Filtrosec}>
            <div>2D</div>
            <div>3D</div>
            <div>IMAX</div>
        </div>
    )
}