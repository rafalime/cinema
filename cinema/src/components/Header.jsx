import styles from './header.module.css'
import fita from '../_assets/icons/fita.png'
import ajuda from '../_assets/icons/ajuda.png'
import acesso from '../_assets/icons/acesso.png'
import ticket from '../_assets/img/Ticket.png'


import { BrowserRouter, Route, NavLink } from 'react-router-dom'


export function Header() {

    const nome = "C{IN}EMA"

    return (
        <header>
            <NavLink to="/"><div className={styles.Logo}>
                <img src={ticket} alt="" />
                <h1 className={styles.LogoCinema}>{nome}</h1>
            </div></NavLink>
            <nav>
                <NavLink to="/Filmes"><img src={fita} alt="" /></NavLink>
                <NavLink to="/Login"><img src={acesso} alt="" /></NavLink>
                <NavLink to="/FaleConosco"><img src={ajuda} alt="" /></NavLink>
            </nav>
        </header >
    )
}