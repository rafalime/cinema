import styles from './horarios.module.css'
import { NavLink } from 'react-router-dom'
export function Horarios(props) {

    return (
        <div className={styles.horarios}>
            <div className={styles.hours}>
                <span>{props.resolucao}</span>
                <div className={styles.SelectHour}>
                    <NavLink to='/Checkout'><div>15:20</div></NavLink>
                    <NavLink to='/Checkout'><div>17:40</div></NavLink>
                    <NavLink to='/Checkout'><div>20:00</div></NavLink>
                    <NavLink to='/Checkout'><div>22:10</div></NavLink>
                </div>
            </div>
        </div>
    )
}