import right from '../_assets/img/Right.png'
import left from '../_assets/img/Left.png'
import pipoca from '../_assets/img/pipoca.png'
import styles from './ticket.module.css'

export function Ticket() {

    /*  <div className={styles.arrow}>
            <button><img src={left} alt="" /></button>
            <button><img src={right} alt="" /></button>
        </div> */
    // TODO Colocar botões de navegação
    return (
        <div className={styles.base}>

            <div>
                <img src={pipoca} alt="" srcset="" />
                <div>
                    <h1 className={styles.nometicket}>Pipoca (P)</h1>
                    <p className={styles.price}>R$ 11,99</p>
                    <p className={styles.description}>Nada diz "cinema" como o aroma inconfundível de pipoca estourando. E quando se trata de uma porção de pipoca pequena, a magia se torna ainda mais irresistível. Imagine uma explosão de sabores em miniatura, onde cada grão é um pedacinho de felicidade.</p>
                </div>
            </div>
        </div>
    )
}