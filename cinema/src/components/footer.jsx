import styles from './footer.module.css'
import insta from '../_assets/icons/insta.png'
import face from '../_assets/icons/facebook.png'
import Linkedin from '../_assets/icons/linkedi.png'
import Ingresso from '../_assets/img/INgresso.png'

export function Footer() {
    return (
        <footer>
            <div>
                <div className={styles.info}>
                    <div className={styles.enderecos}>
                        <h2>Endereço</h2>
                        <p>Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem, Niterói - RJ CEP: 24210-315 </p>
                    </div>
                    <div className={styles.contato}>
                        <div>
                            <h2>Fale conosco</h2>
                            <p>contato@injunior.com.br</p>
                        </div>

                        <span>
                            <img src={insta} />
                            <img src={face} />
                            <img src={Linkedin} />
                        </span>

                    </div>
                </div>
                <img src={Ingresso} alt="Logo Cinema IN Junior" />
                <iframe width="325" height="244" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=pt-br&amp;q=UFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o+(IC%20UFF)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>
            </div>
            <span><p>© Copyright 2023. IN Junior. Todos os direitos reservados. Niterói, Brasil.</p></span>

        </footer>
    )
}