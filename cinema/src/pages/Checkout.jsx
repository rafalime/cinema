import { Header } from '../components/Header.jsx'
import { Footer } from '../components/footer.jsx'
import styles from './checkout.module.css'
import assento from '../_assets/icons/assento.png'
import { useEffect, useState } from 'react'

export function Checkout() {

    // const url = 'http://localhost:3333/api/filmes'
    // const config = {
    //     method: "GET"
    // }

    // useEffect(() => {
    //     fetch(url, config)
    //     .then(response => response.json())
    //     .then(response => {
    //         console.log(response)
    //     })
    // }, [])

    const [modal, setModal] = useState(false)

    const toggleModal = () => {
        setModal(!modal)
    }

    const [Reservado, setReservado] = useState(false)

    const toggleReservado = () => {
        setReservado(!Reservado)
        window.location.href = 'http://localhost:5173'
    }

    const toggleBoth = () => {
        setModal(!modal)
        setReservado(!Reservado)
    }



    return (
        <div>
            <Header />

            <main className={styles.Checkout}>
                <aside>
                    <div className={styles.ConfirmaFilme}>
                        <img src="https://s3-alpha-sig.figma.com/img/f0ba/a8f0/db65e3c30e85b33c539b26ec5bc1ae77?Expires=1710720000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=jXp2xxNsRhhlkwlUox9LAMhrlay3xZjnqd4Qh291oingF1MNAwGTa6eyTDe~PYGPCd~kgphhv18pqTa2WsTm2wl8xhM~bCN8FvWQQiIr188y0~K6iTDwJrO-FByrKtxxdz-t-X946~V2Ut5aJc9V0qvbkz1Jl4Dadxr89Yl0DhLYy-Sql2Ov0zN3eCY-2nIK~58IkVHIxlV~TBjRNAp9rnoaK8OpAoBxJlYIrfHczUkO2srsQSgahgLEpUUScowTpi2AjGzGbU05JtMNxivcXEgZ5mmRvB4eV-2G8QeQii1XXVfJtkeecDBEoL~EvrD-iplQxDF~sv~GZ-~y~PSJVA__" alt="" />
                        <div>
                            <h1>Besouro Azul</h1>
                            <div>
                                <div><p>2D</p></div>
                                <div><p>15:20</p></div>

                            </div>
                        </div>

                    </div>
                    <div className={styles.AssentosEscolhidos}>
                        <h2>ASSENTOS ESCOLHIDOS</h2>
                        <div className={styles.Escolhidos}>

                        </div>
                        <button>CONFIRMAR</button>
                    </div>
                </aside>

                <div className={styles.Seat}>
                    <div className={styles.Tela}>
                        <p>TELA</p>
                    </div>
                    <div className={styles.Cadeiras}>
                        <div className={styles.Linhas}>
                            <p>A</p>
                            <p>B</p>
                            <p>C</p>
                            <p>D</p>
                            <p>E</p>
                            <p>F</p>
                            <p>G</p>
                            <p>H</p>
                            <p>I</p>
                            <p>J</p>
                        </div>
                        <div className={styles.Fileiras}>
                            <span>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                            </span>
                            <span>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                            </span>
                            <span>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                            </span>
                            <span>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                            </span>
                            <span>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                            </span>
                            <span>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                            </span>
                            <span>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                            </span>
                            <span>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                            </span>
                            <span>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                            </span>
                            <span>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                                <div onClick={toggleModal} className={styles.Cadeira}></div>
                            </span>

                        </div>
                        <div className={styles.Linhas}>
                            <p>A</p>
                            <p>B</p>
                            <p>C</p>
                            <p>D</p>
                            <p>E</p>
                            <p>F</p>
                            <p>G</p>
                            <p>H</p>
                            <p>I</p>
                            <p>J</p>
                        </div>
                    </div>
                    <hr />
                    <div className={styles.Legenda}>
                        <p>LEGENDA</p>
                        <div className={styles.LegendaContainer}>
                            <div>
                                <div className={styles.CadeiraLivre}></div>
                                <p>Disponível</p>
                            </div>
                            <div>
                                <div className={styles.CadeiraSelecionada}></div>
                                <p>Selecionado</p>
                            </div>
                            <div>
                                <div className={styles.CadeiraComprada}></div>
                                <p>Comprado</p>
                            </div>
                        </div>
                    </div>
                </div>

            </main>

            {modal && (
                <div onClick={toggleModal} className={styles.overlay}>
                    <div className={styles.Modal}>
                        <h1>Confirmação de Reserva!</h1>
                        <p>Tem certeza que deseja confirmar a reserva?</p>
                        <div>
                            <button onClick={toggleModal}>CANCELAR</button>
                            <button onClick={toggleBoth}>CONFIRMAR</button>
                        </div>
                    </div>
                </div>
            )}

            {Reservado && (
                <div className={styles.overlay}>
                    <div className={styles.Reservado}>
                        <h1>Reserva Confirmada!</h1>
                        <p>Sua reserva foi confirmada com sucesso para a sessão selecionada.</p>
                        <p>Estamos felizes em tê-lo conosco para essa experiência cinematográfica. Prepare-se para se envolver em uma jornada emocionante na tela grande!</p>
                        <button onClick={toggleReservado}>X</button>
                    </div>
                </div>
            )}





            <Footer />
        </div>
    )
}