import styles from './filmes.module.css'
import { Footer } from '../components/footer.jsx'
import { Header } from '../components/Header.jsx'
import { Filtro } from '../components/Filtro.jsx'
import { CardFilme } from '../components/CardFilme.jsx'
import arrowL from '../_assets/icons/arrowL.png'
import arrowR from '../_assets/icons/arrowR.png'

export function Filmes() {

    return (
        <div className={styles.ListaFilmes}>
            <Header />

            <Filtro />
            <h1>Filmes</h1>
            <div className={styles.ContainerDeFilmes}>
                <div className={styles.CardFilmeContainer}>
                    <CardFilme genero="Ação, Aventura" url='https://s3-alpha-sig.figma.com/img/f0ba/a8f0/db65e3c30e85b33c539b26ec5bc1ae77?Expires=1710720000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=jXp2xxNsRhhlkwlUox9LAMhrlay3xZjnqd4Qh291oingF1MNAwGTa6eyTDe~PYGPCd~kgphhv18pqTa2WsTm2wl8xhM~bCN8FvWQQiIr188y0~K6iTDwJrO-FByrKtxxdz-t-X946~V2Ut5aJc9V0qvbkz1Jl4Dadxr89Yl0DhLYy-Sql2Ov0zN3eCY-2nIK~58IkVHIxlV~TBjRNAp9rnoaK8OpAoBxJlYIrfHczUkO2srsQSgahgLEpUUScowTpi2AjGzGbU05JtMNxivcXEgZ5mmRvB4eV-2G8QeQii1XXVfJtkeecDBEoL~EvrD-iplQxDF~sv~GZ-~y~PSJVA__' sinopse="Quando um escaravelho alienígena se funde com seu corpo, Jaime ganha uma armadura tecnológica que lhe concede superpoderes incríveis." nome="Besouro Azul" classe="12" diretor="Angel Manuel Soto" />
                    <CardFilme genero="Aventura, Animação" url='https://s3-alpha-sig.figma.com/img/ae62/e019/30326c1239e1b8c291237a1dee6a8409?Expires=1710720000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=EJXZqB-FQSuXb28FHus1krf85S4CvE4IVq4krjvzAgs8H4W0OtkN4LvgvQvbH3K5HaqWOF0L4mGLbOFii8cAhXeImFFg2SfublOKDaxKAXNLz4pDQ33HNZCbqWHiyf4ugR-~4y8eL~M~fBabgwcigudZTL-96TidAK0vA9UTEWvUsylZRZpXDbXZzKUOChQMTAwgHSIb5mElLDx~TjXfNo5zNGecN4cv3KrQOIJ1q4ceW4D-5uD9yJ-XgFEYL-1A5LlWcFAazuG9EQ3eqHJosuWcGwqOYcMt712kor3fxGgnL8RB1ykWF2qZy3ysw8ssPjBYKODOl7oMUuoXPHpzFw__' sinopse="A trama segue as aventuras de uma dupla improvável, Ember e Wade, em uma cidade onde os moradores do fogo, água, terra e ar vivem juntos." nome="elementos" classe="L" diretor="Peter Sohn" />
                </div>
                <div className={styles.CardFilmeContainer}>
                    <CardFilme genero="Ação" url='https://s3-alpha-sig.figma.com/img/5f59/9b67/fd254e8a4c1fb5bfd439f87c43e4ac42?Expires=1710720000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=iSWw1haUqqtcAtfWaz9t2jOGvHVyKKi-Co52N1xdPEeWQY00t5dIVjlYhhOEdSSC8vs8vSJNDMMJPExL-g2fH0~x575D2R32fl6VbqwdfJPy~VGIGzdWstFuICra6l5WS2Qj3Cu3cenl-mS6XyA3H25-xz4gOiV7rmwF8TjR90dYve1OW0xnB9oLuOib-O9AutZZBedt~mOjNi3jVhH3tQBPVpQtfbtp4e59a9-UrQ-1YAy~ETLH3~aPWMrxppZTUenAtHaJ4nnlnUEmcAP5eoSMgJ822qoL7ljTvwpQCeCwC1NLKsuk-s6gC1L0nIa2wMIzmyhs3UTrjyz2ZHJV8A__' sinopse="Ethan Hunt e sua equipe da IMF embarcam em uma perigosa missão para rastrear uma arma terrível que ameaça a humanidade." nome="missão: impossível -acerto de contas parte 1" classe="14" diretor="Christopher McQuarrie" />
                    <CardFilme genero="Comédia, Aventura" url='https://s3-alpha-sig.figma.com/img/cc5f/e61b/2f618829e3047444c2269a7faefa6491?Expires=1710720000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=JfqCICYms6G2CUY4RECnAVtKytpuUKcVy7EBfz2gOspb6tkKOEwrJ31jPSMAYkpCIiW95dyerWLeWcOv7vRYOB03KSUrn3kCp2vJ6M3kSBLtTM54WFbx97MX4hQ4aS~QO01n1LD0qTacQVXaCvy3ZA4pMm7n6PekRFauRhKpU8tzwspVHrlSrERE5VxHn9YtNl3fcgSh6RpH-j232NIg1yG3R9VkHGYrgC60gV4nyzGyUAsJhF8YHw2ItjUyxYcIfehV1tTAp1X6oMT4M4oVy63RtRxhVnT4Ypp32FMFM~m85Yr5zNKf6EXuh4GuBMMB3BeebZbmauguqQ24FLrg7g__' sinopse="No mundo mágico das Barbies, 'BarbieLand', uma das bonecas começa a perceber que não se encaixa como as outras" nome="Barbie" classe="12" diretor="Greta Gerwig" />
                </div>
                <div className={styles.CardFilmeContainer}>
                    <CardFilme genero="Ação, Fantasia, Animação" url='https://s3-alpha-sig.figma.com/img/c10d/c7f6/1f7a6e6caf4c7305dfb786062f22d806?Expires=1710720000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=qFV5zNtbxbrS3NJ8RTHSFbpqNpjgNQP6U161mKYlt5fjqQ4RFc4RwnMrXmdGf28Y2DUazOK2a1q48xE2J8EAy87goywo-UOAbDX2w0ehf57y0CSj18bONOQc9MjfG-yzq2b6rpibW86r5K9zyyZIoGpMuKvHAOazwplszj8hi-xRM99WBa~VzOTBu0N5JshkFl-twZriB5pjZc2z-HrG8NTQyT6YKf3GN5e8baAdhy-dx~56LhQYfV3GFVSeyG8NT6SPmd~W7oWh-6U8veMIwdkZ04pnaaZKE8rVlIrqUsO0De5cxg5l7TUlBxyRNUrSK5i4fNHb4QX~CtvCeI~gjA__' sinopse="O amigão da vizinhança viaja através do multiverso, para enfrentar um vilão mais poderoso do que qualquer coisa que já tenham encontrado." nome="Homem-Aranha: através do aranhaverso" classe="10" diretor="Joaquim Dos Santos, Kemp Powers" />
                    <CardFilme genero="Drama" url='https://s3-alpha-sig.figma.com/img/3398/4419/9993791c13f5ab801487954015564088?Expires=1710720000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=GM6ddlBSVD5UEfVGgDCMcKgAWwvjNphvw9FYnEO4~NFzwdN61LG3uCIn~BdAF28VzJoIO5cd4MGVXRzmD8OhKJ1w0qHN0tk5lo1xNC1gTcTOJE5sKQCreXlCS3C3oczbcBZsTsUgs~fyBvEDjwapXRBolo0-XHJIXbPJXCGu6qGy5biezyLizSmVhnkw1Kl5Iagtp-8pBXcZH~HFy96qWwRMCnN1h7~pcDMx~su3T00LpvxTSwmp98ThvsKxgN3p5rFwpGv35MtK~xAEYkP7H3xhopn4sTdrK2wD2bkUwajKXn6NPky5aFHKapxFD1pkdbI7cBqFR0fqF8Y3LtzOgA__' sinopse="A história do físico americano Julius Robert Oppenheimer e seu papel no desenvolvimento da bomba atômica durante a Segunda Guerra Mundial." nome="oppenheimer" classe="16" diretor="Christopher Nolan" />
                </div>
            </div>

            <div className={styles.FilmesNav}>
                <div className={styles.arrowL}><img src={arrowL} alt="" /></div>
                <div><p>1</p></div>
                <div><p>2</p></div>
                <div><p>...</p></div>
                <div><p>4</p></div>
                <div><p>5</p></div>
                <div className={styles.arrowR}><img src={arrowR} alt="" /></div>
            </div>

            <Footer />
        </div>

    )
}