import { Header } from '../components/Header.jsx'
import { Footer } from '../components/footer.jsx'
import { Ticket } from '../components/Ticket.jsx'
import movieImg from '../_assets/img/Oppenheimer.png'
import styles from './home.module.css'
import { NavLink } from 'react-router-dom'


export function Home() {
    // TODO Corrigir overflow da página, em especial da seção de filmes em cartaz

    const config = {
        method: "GET"
    }
    const url = 'http://localhost:3333/api/filmes'

    fetch(url, config)
        .then(response => {
            if (!response.ok) {
                throw new Error('Erro na requisição.');
            }
            return response.json();
        })
        .then(data => {
            console.log('Dados recebidos:', data);
        })
        .catch(error => {
            console.error('Erro:', error);
        });


    return (
        <div className={styles.Homediv}>
            <Header />
            <div className={styles.intro}>
                <h1>Transformando Filmes em Experiências Personalizadas</h1>
                <h2>Reserve Seu Assento e Viva a Magia do Cinema!</h2>
            </div>
            <main className={styles.homeMain}>
                <div className={styles.tickets}>
                    <Ticket />
                    <Ticket />
                    <Ticket />

                </div>

                <section>
                    <h1>Em cartaz</h1>
                    <span className={styles.movieList}>
                        <div className={styles.MovieCard}>
                            <img src={movieImg} alt="" />
                            <h2>Oppenheimer</h2>
                            <button>Sessões disponíveis</button>
                        </div>
                        <div className={styles.MovieCard}>
                            <img src={movieImg} alt="" />
                            <h2>Oppenheimer</h2>
                            <button>Sessões disponíveis</button>
                        </div>
                        <div className={styles.MovieCard}>
                            <img src={movieImg} alt="" />
                            <h2>Oppenheimer</h2>
                            <button>Sessões disponíveis</button>
                        </div>
                        <div className={styles.MovieCard}>
                            <img src={movieImg} alt="" />
                            <h2>Oppenheimer</h2>
                            <button>Sessões disponíveis</button>
                        </div>
                        <div className={styles.MovieCard}>
                            <img src={movieImg} alt="" />
                            <h2>Oppenheimer</h2>
                            <button>Sessões disponíveis</button>
                        </div>
                    </span>
                    <div>
                        <NavLink to='/Filmes' ><a href="">Ver mais</a></NavLink>
                    </div>

                </section>
            </main>
            <Footer />
        </div>
    )
}