import styles from './login.module.css'
import { Footer } from '../components/footer.jsx'
import { Header } from '../components/Header.jsx'
import ticket from '../_assets/img/Ticket.png'
import {NavLink} from 'react-router-dom'

export function Login() {

    const nome = "C{IN}EMA"

    return (
        <div>
            <Header />
            <main className={styles.Login}>
                <div className={styles.LoginDescription}>
                    <span>
                        <img src={ticket} alt="" />
                        <h1>{nome}</h1>
                    </span>
                    <p>Escolha suas sessões, reserve seus lugares e entre de cabeça em narrativas que cativam e emocionam. Este é o nosso convite para você vivenciar o cinema de maneira única. Nossa página de login é a porta de entrada para essa experiência excepcional, e estamos empolgados para compartilhar cada momento cinematográfico com você.</p>
                </div>
                <div className={styles.LoginForm}>
                    <h1>Login</h1>

                    <form action="">
                        <h2>Faça login e garanta o seu lugar na diversão</h2>
                        <input type="text" placeholder='Usuário ou E-mail' />
                        <input type="password" placeholder='Senha' />
                        <div><a href="">Esqueci minha senha</a></div>
                        <button className={styles.LoginButton}>ENTRAR</button>
                        <hr></hr>
                        <NavLink to='/Registro'><button className={styles.SingInButton}>CADASTRE-SE</button></NavLink>
                    </form>

                </div>

            </main>

            <Footer />
        </div>
    )
}