import styles from './sessoes.module.css'
import { Footer } from '../components/footer.jsx'
import { Header } from '../components/Header.jsx'
import { Horarios } from '../components/Horarios.jsx'
import { Filtrosec } from '../components/Filtrosec.jsx'


export function Sessoes() {



    return (
        <div>
            <Header />
            <section className={styles.Ofilme}>
                <img src="https://s3-alpha-sig.figma.com/img/f0ba/a8f0/db65e3c30e85b33c539b26ec5bc1ae77?Expires=1710720000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=jXp2xxNsRhhlkwlUox9LAMhrlay3xZjnqd4Qh291oingF1MNAwGTa6eyTDe~PYGPCd~kgphhv18pqTa2WsTm2wl8xhM~bCN8FvWQQiIr188y0~K6iTDwJrO-FByrKtxxdz-t-X946~V2Ut5aJc9V0qvbkz1Jl4Dadxr89Yl0DhLYy-Sql2Ov0zN3eCY-2nIK~58IkVHIxlV~TBjRNAp9rnoaK8OpAoBxJlYIrfHczUkO2srsQSgahgLEpUUScowTpi2AjGzGbU05JtMNxivcXEgZ5mmRvB4eV-2G8QeQii1XXVfJtkeecDBEoL~EvrD-iplQxDF~sv~GZ-~y~PSJVA__" alt="" />
                <div>
                    <span>
                        <h1>Besouro Azul</h1>
                        <div><p>12</p></div>
                    </span>
                    <p>Ação, Aventura</p>
                    <p>Quando um escaravelho alienígena se funde com seu corpo, Jaime ganha uma armadura tecnológica que lhe concede superpoderes incríveis.</p>
                    <div className={styles.FiltrosSessao}>
                        <select name="" id="">
                            <option value="">Cidade</option>
                        </select>
                        <select name="" id="">
                            <option value="">Bairro</option>
                        </select>
                    </div>
                </div>

            </section>
            <main className={styles.SessoesFilme}>
                <Filtrosec />
                <Horarios resolucao='2D' />
                <Horarios resolucao='3D' />
                <Horarios resolucao='IMAX' />
            </main>


            <Footer />
        </div>
    )
}