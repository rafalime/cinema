import { Header } from "../components/Header"
import { Footer } from "../components/footer"
import styles from "./faleconosco.module.css"
import { useState } from 'react'

export function FaleConosco() {
    
    //TODO Modal só deve aparecer se formulário estiver preenchido
    const [modal, setModal] = useState(false)

    const toggleModal = () => {
        setModal(!modal)
    }

    return (
        <div>
            <Header />
            <main>
                <form action="">
                    <h1>Contato</h1>
                    <div>
                        <h2>Encontrou algum problema?</h2>
                        <h2>Envie uma mensagem!</h2>
                    </div>

                    <fieldset>
                        <input type="text" placeholder="Nome Completo" required />
                        <input type="text" placeholder="Assunto" required />
                        <textarea name="" id="" cols="30" rows="10" placeholder="Descrição detalhada" required></textarea>
                        <button onClick={toggleModal} className={styles.modalbtn}>ENVIAR</button>
                    </fieldset>

                </form>
            </main>

            {modal && (
                <div onClick={toggleModal} className={styles.modal}>
                    <h1>Mensagem Enviada!</h1>
                    <h2>Obrigado por compartilhar suas observações conosco.</h2>
                    <p>Sua contribuição é fundamental para melhorarmos continuamente a sua experiência em nosso site. Valorizamos seu tempo e dedicação ao relatar esse problema.</p>
                    <button onClick={toggleModal}>X</button>
                </div>
            )}

            


            <Footer />
        </div>
    )
}