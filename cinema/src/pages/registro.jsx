import { Header } from '../components/Header.jsx'
import { Footer } from '../components/footer.jsx'
import styles from './registro.module.css'

export function Registro() {
    return (
        <div>
            <Header />
            <main>
                <div>
                    <h1>Junte-se à Comunidade Cinematográfica! </h1>
                    <h1>Cadastre-se Aqui!</h1>
                    <p>Seja bem-vindo à nossa comunidade apaixonada pelo mundo do cinema. Ao fazer parte do nosso espaço digital, você está prestes a mergulhar em uma experiência cinematográfica única, onde a magia das telonas ganha vida com um toque moderno.</p>
                    <p>Nosso formulário de cadastro é o primeiro passo para embarcar nessa jornada emocionante. Ao preenchê-lo, você se tornará um membro da nossa comunidade, onde amantes do cinema se reúnem para compartilhar o entusiasmo, as emoções e as histórias que permeiam cada cena.</p>
                </div>
                <form action="">
                    <h1>Registre-se</h1>
                    <fieldset>
                        <input type="text" name='Nome' placeholder='Nome' required />
                        <input type="text" name='Sobrenome' placeholder='Sobrenome' required />
                        <input type="number" name='CPF' placeholder='CPF' required />
                        <input type="text" name='Nascimento' placeholder='Data de Nascimento' required />
                        <input type="text" name='Usuario' placeholder='Nome de Usuário' required />
                        <input type="email" name='Email' placeholder='E-mail' required />
                        <input type="password" name='Senha' placeholder='Senha' required />
                        <input type="password" name='Confirmar' placeholder='Confirmar Senha' required />
                    </fieldset>
                    <button className={styles.btn}>Registrar</button>
                </form>
            </main>
            <Footer />
        </div>

    )
}